FROM node:alpine AS builder
WORKDIR /app
COPY package.json .
RUN npm install --silent
COPY . .
RUN npm run build

FROM nginx AS runtime
COPY --from=builder /app/build /usr/share/nginx/html
